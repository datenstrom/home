# .zshrc

# Disable legacy Software Flow Control (XON/XOFF flow control)
# Do not freeze the terminal when `C-s` is pressed.
#
# $- contains the current shell options. In [[ ... ]], the right hand side of
# a = is interpreted as a pattern if not quoted. Therefore, *i* means i
# possibly preceded or followed by anything. In other words, it checks wheter
# the i option is present, i.e. whether the current shell is interactive.
[[ $- == *i* ]] && stty -ixon

# Detect OS Distribution
source $HOME/.local/lib/os-detection
ZSH_CUSTOM=$HOME/.local/share/oh-my-zsh

# Detect if device is a laptop by hostname
# Useful for setting prompts with battery status
laptops=('void' 'rogue' 'nb505')
user="$USER"
hostname=$(hostname)
for host in $laptops; do
    if [ host = hostname ]; then
        laptop=true
    fi
done

setopt histignorealldups    # Ignore duplicates
bindkey -v                  # Use vim keybindings even if our EDITOR is set to emacs

# Path to oh-my-zsh installation
if [ "$OS_NAME" = "NixOS" ] ; then
    export ZSH=/run/current-system/sw/share/oh-my-zsh
else
    export ZSH=$HOME/.oh-my-zsh
fi

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
#
# Favorites:
#
#  * sunrise
#  * miloshadzic
#  * norm
#  * lambda
#  * mrtazz
#  * pygmalion
#  * sorin
#  * smt
#  * agnoster
#  * amuse
#  * avit
ZSH_THEME="sunrise"
DEFAULT_USER="datenstrom"

# Prompt settings
CASE_SENSITIVE="true"           # case-sensitive completion
ENABLE_CORRECTION="true"        # Enable auto correction

# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
HIST_STAMPS="dd.mm.yyyy"

plugins=(
    git
    vi-mode
    rust 
    colored-man-pages
    docker
    docker-compose
)

# Python pip completion for zsh
function _pip_completion {
  local words cword
  read -Ac words
  read -cn cword
  reply=( $( COMP_WORDS="$words[*]" \
             COMP_CWORD=$(( cword-1 )) \
             PIP_AUTO_COMPLETE=1 $words[1] ) )
}
compctl -K _pip_completion pip

# Init oh-my-zsh
source $ZSH/oh-my-zsh.sh

# If the Nix package manager is installed source it
NIX_SOURCE="$HOME/.nix-profile/etc/profile.d/nix.sh"
[ -f "$NIX_SOURCE" ] && source "$NIX_SOURCE"

# Load the shell dotfiles, and then some:
# * ~/.path can be used to extend `$PATH`
# * ~/.extra can be used for other settings you don’t want to commit
for file in ~/.local/lib/{path,exports,settings,env,aliases,colors,utilities,extra}; do
	[ -r "$file" ] && [ -f "$file" ] && source "$file";
done;
unset file;

unsetopt share_history
unsetopt correct_all
