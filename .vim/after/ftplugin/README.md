# Local filetype settings

To get a list of known filetypes enter `:setfiletype ` (with a space afterwards), then press `Ctrl-d`, or check out [the source][1].

[1]: https://github.com/vim/vim/blob/master/runtime/filetype.vim
