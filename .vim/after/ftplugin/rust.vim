" File: rust.vim
" Author: Derek Goddeau <datenstrom@protonmail.com>
" Description: Rust specific file settings

let g:rust_fold = 1
let g:rustfmt_autosave = 1
