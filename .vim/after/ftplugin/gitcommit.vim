" File: gitcommit.vim
" Author: Derek Goddeau <datenstrom@protonmail.com>
" Description: Git Commit specific file settings

setlocal spell          " Spellcheck
setlocal linebreak      " Only wrap at a character in the `breakat` option
setlocal nolist         " list must be disabled for linebreak to work
setlocal textwidth=72   " Automatic word wrapping at 72
