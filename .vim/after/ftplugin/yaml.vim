" File: yaml.vim
" Author: Derek Goddeau <datenstrom@protonmail.com>
" Description: YAML specific file settings

" Two space indentation
setlocal shiftwidth=2
setlocal tabstop=2
setlocal expandtab
