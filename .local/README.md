# .local

A place for installation of local files as defined in the [XDG Base Directory Spec][1] and [Python PEP-370][2]. See also the [Filesystem Hierarchy Standard][3]. This directory is shared with other applications and care must be taken for collisions and changes to files must be performed individually.

[1]: https://standards.freedesktop.org/basedir-spec/basedir-spec-latest.html
[2]: https://www.python.org/dev/peps/pep-0370/
[3]: http://www.pathname.com/fhs/pub/fhs-2.3.html#FTN.AEN1450

------------------------------------------------------------------------

The full local hierarchy:

  - `$HOME/.local/bin` Binaries and scripts (executables)
  - `$HOME/.local/etc` Host-specific system configuration for local binaries
  - `$HOME/.local/games` Game binaries
  - `$HOME/.local/include` C header files
  - `$HOME/.local/lib` Libraries
  - `$HOME/.local/man` Manuals
  - `$HOME/.local/sbin` System binaries
  - `$HOME/.local/share` Architecture-independent hierarchy
  - `$HOME/.local/src` Source code
  - `$HOME/.local/tmp` Scratch space for binaries

When running configure, define the local hierarchy for installation by specifying `$HOME/.local` as the prefix for the installation defaults.

```sh
./configure --prefix=$HOME/.local
```
