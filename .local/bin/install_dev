#!/bin/bash
#
# Install dev tools.

export DEBIAN_FRONTEND=noninteractive

source "$HOME/.local/lib/posix_common"

_info "Update the apt package index"
sudo apt-get update

_info "Running custom installs"
install_vim
install_zsh
install_rust
install_keybase
install_docker
install_julia
install_miniconda


_info "Running apt installs"
sudo apt-get install --yes locales-all \
                           terminator \
                           keepass2 \
                           gnupg2 \
                           tmux \
                           python3-pip \
                           openvpn \
                           openvpn-systemd-resolved \
                           network-manager-openvpn-gnome \
                           python3-neovim # python3-pip & neovim for deoplete

_info "Checking OS type"
if [ -f /etc/os-release ]; then
    . /etc/os-release
    OS=$NAME
    _info "Found: $OS"
else
    _warning "Unable to detect OS"
fi


if [ "$OS" == "Pop!_OS" ]; then
    _info "Installing Pop! OS specific software"
    sudo apt-get install --yes alacritty
else
    _warning "Distro is not Pop! OS skipping packages from System76 PPA"
fi

_info "Installing packages from Cargo"
cargo install ripgrep \
              tokei \
              xcp \
              tealdeer \
              exa \
              bat

_info "Installation complete"

_info "Configuring pinentry-curses as default pinentry"
sudo update-alternatives --set pinentry /usr/bin/pinentry-curses

# Do not fail if headless
_info "Configuring Gnome workspaces"
gsettings set org.gnome.mutter workspaces-only-on-primary false || true

# TODO: Replace with fw?
_info "Configuring workspace"
WORKSPACE="$HOME/workspace"
GITLAB="$WORKSPACE/src/gitlab.com"
GITHUB="$WORKSPACE/src/github.com"
mkdir -p "$GITLAB/datenstrom" \
         "$GITLAB/starshell" \
         "$GITHUB/sevro" \
         "$WORKSPACE/scratch" \
         "$WORKSPACE/datasets"

_info "Running keybase"
run_keybase
