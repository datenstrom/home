# ~/.bashrc: executed by bash(1) for non-login shells.

# https://www.gnu.org/software/bash/manual/html_node/The-Shopt-Builtin.html
shopt -s autocd         # The name of a directory will implicitly `cd` into it
shopt -s cdspell        # Correct small typos when using `cd` 
shopt -s checkjobs      # Notify if background processes are running on exit
shopt -s checkwinsize   # If necessary, updates the values of LINES and COLUMNS
shopt -s dirspell       # Correct small directory name completion errors
shopt -s histappend     # Append to history; default is overwrite on exit

# https://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html
set -o vi               # Enable vi-style command line editing interface

# Disable legacy Software Flow Control (XON/XOFF flow control)
# Do not freeze the terminal when `C-s` is pressed.
#
# $- contains the current shell options. In [[ ... ]], the right hand side of
# a = is interpreted as a pattern if not quoted. Therefore, *i* means i
# possibly preceded or followed by anything. In other words, it checks wheter
# the i option is present, i.e. whether the current shell is interactive.
[[ $- == *i* ]] && stty -ixon

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# If the Nix package manager is installed source it
NIX_SOURCE="$HOME/.nix-profile/etc/profile.d/nix.sh"
[ -f "$NIX_SOURCE" ] && source "$NIX_SOURCE"

# Load the shell dotfiles, and then some:
# * ~/.path can be used to extend `$PATH`.
# * ~/.extra can be used for other settings you don’t want to commit.
for file in ~/.local/lib/{path,exports,settings,env,aliases,colors,utilities,bash-utilities,extra}; do
    [ -r "$file" ] && [ -f "$file" ] && source "$file";
done;
unset file;

# Default to zsh
if [ $SHELL != $(which zsh) ]; then
    _warning "Default shell is bash, changing to zsh" 
    chsh -s $(which zsh)
    _info "Restart required for default shell settings to take effect"
fi
. "$HOME/.cargo/env"
