{ pkgs, ... }:

{

  environment.systemPackages = with pkgs; [
    bitcoin
    altcoins.litecoin
    go-ethereum
  ];

}
