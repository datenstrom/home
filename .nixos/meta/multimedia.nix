{ pkgs, ... }:

{

  environment.systemPackages = with pkgs; [
    gimp
    blender
    darktable
    openshot-qt
    subtitleeditor
    avidemux
    inkscape
    shotcut
  ];

}
