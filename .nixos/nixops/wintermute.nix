{
  network.description = "Personal server configurations";

  brute =
    { config, lib, pkgs, ... }:
    {

     deployment.targetHost = "172.17.1.11";
     imports =
       [
         ./hosts/brute/brute.nix
         ./services/gitlab-runner/gitlab-runner.nix
       ];
       services.gitlab-runner.configFile = ./services/gitlab-runner/x86/quad-core/config.toml.secret;

    };


/*
  rocinante =
    { config, lib, pkgs, ... }:
    {

      deployment.targetHost = "192.168.1.42";
      imports =
        [
          ./hosts/rocinante/rocinante.nix
        ];

    };
*/

  truck =
    { config, lib, pkgs, ... }:
    {

      deployment.targetHost = "172.17.1.100";
      imports =
        [
          ./hosts/truck/truck.nix
          ./services/media/media.nix
          #./services/vpn/pia.nix
        ];

    };

/*
  mack =
    { config, lib, pkgs, ... }:
    {

      deployment.targetHost = "172.17.1.101";
      imports =
        [
          ./hosts/truck/truck.nix
          ./services/media.nix
          #./services/vpn/pia.nix
        ];

    };

  dynamite =
    { config, lib, pkgs, ... }:
    {

      deployment.targetHost = "172.17.1.40";
      imports =
        [
          ./hosts/dynamite/dynamite.nix
        ];

    };

  squadron =
    { config, lib, pkgs, ... }:
    {

      deployment.targetHost = "172.17.1.41";
      imports =
        [
          ./hosts/squadron/squadron.nix
        ];

    };
*/

}
