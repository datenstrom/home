# GitLab Runner

**Add the proper `config.toml` file in the nixops deploy script.**

> **NOTE:** This may not be the best setup, it looks like `gitlab-runner register` still needs to be ran on individual machines to get a token for them. This will need to be investigated further.

The GitLab runner `config.toml` file requires some settings that are hardware specific, and items in this _services_ section must not be tied to specific machines. The `config.toml` files must also not be put in each machines file in the _hosts_ section since they may not always be a gitlab-runner. So config files are instead made for classes of machines, such as:

- `arm/rpi3`
- `x86/quad-core`
- `x86/dual-core`

So that they may be reused on machines with similar specs.

## git-crypt

GitLab stores secrets in the runner config files, to manage this they are encrypted using [git-crypt](https://www.agwa.name/projects/git-crypt/).
