{ config, pkgs, lib, ... }:

with lib;

let
  cfg = config.services.radarr;
in
{
  options = {
    services.radarr = {
      openFirewall = mkOption {
        type = types.bool;
        default = false;
        description = ''
          Open ports in the firewall for the media server
        '';
      };

      user = mkOption {
        type = types.str;
        default = "radarr";
        description = "User account under which radarr runs.";
      };

      group = mkOption {
        type = types.str;
        default = "radarr";
        description = "Group under which radarr runs.";
      };

    };
  };

  config = mkIf cfg.enable {
    systemd.services.radarr = pkgs.lib.mkForce {
      description = "Radarr";
      after = [ "network.target" ];
      wantedBy = [ "multi-user.target" ];
      preStart = ''
        test -d /var/lib/radarr/ || {
          echo "Creating radarr data directory in /var/lib/radarr/"
          mkdir -p /var/lib/radarr/
        }
        chown -R ${cfg.user}:${cfg.group} /var/lib/radarr/
        chmod 0700 /var/lib/radarr/
      '';

      serviceConfig = {
        Type = "simple";
        User = cfg.user;
        Group = cfg.group;
        PermissionsStartOnly = "true";
        ExecStart = "${pkgs.radarr}/bin/Radarr";
        Restart = "on-failure";
      };
    };

    networking.firewall = mkIf cfg.openFirewall {
      allowedTCPPorts = [ 7878 ];
    };

    users.users = mkIf (cfg.user == "radarr") {
      radarr = pkgs.lib.mkForce {
        uid = config.ids.uids.radarr;
        home = "/var/lib/radarr";
        group = cfg.group;
        extraGroups = [ "media" ];
      };
    };

    users.groups = mkIf (cfg.group == "radarr") {
      radarr = pkgs.lib.mkForce {
        gid = config.ids.gids.radarr;
      };
    };
  };
}
