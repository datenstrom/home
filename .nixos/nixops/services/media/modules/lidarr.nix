{ config, pkgs, lib, ... }:

with lib;

let
  cfg = config.services.lidarr;
in
{
  options = {
    services.lidarr = {
      enable = mkEnableOption "Radarr";

      openFirewall = mkOption {
        type = types.bool;
        default = false;
        description = ''
          Open ports in the firewall for the media server
        '';
      };

      user = mkOption {
        type = types.str;
        default = "lidarr";
        description = "User account under which lidarr runs.";
      };

      group = mkOption {
        type = types.str;
        default = "lidarr";
        description = "Group under which lidarr runs.";
      };

    };
  };

  config = mkIf cfg.enable {
    systemd.services.lidarr = {
      description = "Lidarr";
      after = [ "network.target" ];
      wantedBy = [ "multi-user.target" ];
      preStart = ''
        test -d /var/lib/lidarr/ || {
          echo "Creating lidarr data directory in /var/lib/lidarr/"
          mkdir -p /var/lib/lidarr/
        }
        chown -R ${cfg.user}:${cfg.group} /var/lib/lidarr/
        chmod 0700 /var/lib/lidarr/
      '';

      serviceConfig = {
        Type = "simple";
        User = cfg.user;
        Group = cfg.group;
        PermissionsStartOnly = "true";
        ExecStart = "${pkgs.lidarr}/bin/lidarr";
        Restart = "on-failure";
      };
    };

    networking.firewall = mkIf cfg.openFirewall {
      allowedTCPPorts = [ 8686 ];
    };

    users.users.lidarr = pkgs.lib.mkForce {
      uid = 9000;
      home = "/var/lib/lidarr";
      group = cfg.group;
      extraGroups = [ "media" ];
    };

    users.groups = mkIf (cfg.group == "lidarr") {
      lidarr = pkgs.lib.mkForce {
        gid = config.ids.gids.lidarr;
      };
    };
  };
}
