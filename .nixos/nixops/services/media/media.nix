# Configuration file for a media server
#
# NOTE: All large storage is configured to be in `$mediaDir`
#
# FIXME: `$mediaDir` is set to `/var/lib/plex` and should be
#        `/var/lib/media`. Change at a time when most sonarr
#        and radarr downloads are complete to avoid broken paths.

{ config, pkgs, lib, ... }:

let
  mediaDir = "/var/lib/plex";
  plexDir = "${mediaDir}/plex";
  embyDir = "${mediaDir}/emby";
  transmissionDir = "${mediaDir}/transmission";
in
{

  /*
  nixpkgs.overlays = [
    (import ../overlays/lidarr.nix)
  ];
  */

  imports = [
    ./modules/radarr.nix
    ./modules/sonarr.nix
    #./modules/lidarr.nix
    #./modules/deluge.nix
    ./modules/transmission.nix
  ];

  users.groups.media.members = [ "plex" "sonarr" "radarr" "lidarr" "transmission" ];
  users.groups.transmission.members = [ "sonarr" "radarr" "lidarr" "transmission" ];

  # FIXME: `allowUnfree` Required for Plex
  #
  # Try Emby as a Plex replacement.
# services.emby = {
#   enable = true;
#   group = "media";
#   openFirewall = true;
#   dataDir = "${embyDir}";
# };
  nixpkgs.config.allowUnfree = true;
  services.plex = {
    enable = true;
    group = "media";
    openFirewall = true;
    dataDir = "${plexDir}";
  };

  services.radarr = {
    enable = true;
    group = "media";
    openFirewall = true;
  };

  services.sonarr = {
    enable = true;
    group = "media";
    openFirewall = true;
  };

  /*
  services.lidarr = {
    enable = true;
    group = "media";
    openFirewall = true;
  };
  */

# services.deluge = {
#   enable = true;
#   group = "media";
#   openFirewall = true;
#   web.enable = true;
# };

  services.transmission = {
    enable = true;
    openFirewall = true;
    home = "${transmissionDir}";
    settings = {
      alt-speed-down =  25000;
      alt-speed-enabled = false;
      alt-speed-time-begin = 540;
      alt-speed-time-day = 127;
      alt-speed-time-enabled = false;
      alt-speed-time-end = 1020;
      alt-speed-up = 50;
      bind-address-ipv4 = "0.0.0.0";
      bind-address-ipv6 = "::";
      blocklist-enabled = false;
      blocklist-url = "http://www.example.com/blocklist";
      cache-size-mb = 8;
      dht-enabled = true;
      download-dir = "${transmissionDir}/Downloads";
      download-queue-enabled = true;
      download-queue-size = 10;
      encryption = 1;
      idle-seeding-limit = 30;
      idle-seeding-limit-enabled = true;
      incomplete-dir = "${transmissionDir}/.incomplete";
      incomplete-dir-enabled = true;
      lpd-enabled = false;
      message-level = 2;
      peer-congestion-algorithm = "";
      peer-id-ttl-hours = 6;
      peer-limit-global = 200;
      peer-limit-per-torrent = 50;
      peer-port = 51413;
      peer-port-random-high = 65535;
      peer-port-random-low = 49152;
      peer-port-random-on-start = false;
      peer-socket-tos = "default";
      pex-enabled = true;
      port-forwarding-enabled = true;
      preallocation = 1;
      prefetch-enabled = true;
      queue-stalled-enabled = true;
      queue-stalled-minutes = 30;
      ratio-limit = 1;
      ratio-limit-enabled = true;
      rename-partial-files = true;
      rpc-authentication-required = false;
      rpc-bind-address = "0.0.0.0";
      rpc-enabled = true;
      rpc-password = "{7c4e19158baa5c43c84a8569adb768f20fefba3fQb4gSAgq";
      rpc-port = 9091;
      rpc-url = "/transmission/";
      rpc-username = "";
      rpc-whitelist = "127.0.0.1,172.17.1.4,192.168.1.*";
      rpc-whitelist-enabled = true;
      scrape-paused-torrents-enabled = true;
      script-torrent-done-enabled = false;
      script-torrent-done-filename = "";
      seed-queue-enabled = false;
      seed-queue-size = 10;
      speed-limit-down = 100;
      speed-limit-down-enabled = false;
      speed-limit-up = 100;
      speed-limit-up-enabled = true;
      start-added-torrents = true;
      trash-original-torrent-files = false;
      umask = 2;
      upload-slots-per-torrent =  14;
      utp-enabled = true;
    };
  };
}
