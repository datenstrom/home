# Configuration file for Acme
{ config, pkgs, lib, ... }:

{

  networking.hostName = "acme";
  time.timeZone = "America/New_York";

  imports =
    [
      <nixpkgs/nixos/modules/profiles/headless.nix>
      ./acme-hardware.nix
      ../baseline.nix
    ];

  environment.systemPackages = with pkgs; [
    vim
  ];

  networking.firewall.allowPing = true;
  networking.firewall.allowedTCPPorts = [
    22   # SSH
    80   # HTTP
    443  # HTTPS
    3000 # Gogs
    8080 # GitLab
  ];

  programs.bash.enableCompletion = true;

  services.gogs.enable = true;

  # Broken in 17.09
  #services.gitlab = {
  #  enable = true;
  #  port = 8080;
  #  databasePassword = builtins.readFile ./gitlab-database.password;
  #  secrets = {
  #    secret = builtins.readFile ./gitlab.secret;
  #    otp = builtins.readFile ./gitlab.otp;
  #    db = builtins.readFile ./gitlab.db;
  #    jws = builtins.readFile ./gitlab.pem;
  #  };
  #};
}
