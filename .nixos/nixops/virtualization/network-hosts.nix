let
  machine = {host, port ? 22, rootUuid, swapUuid, hostNic, guests ? {}}: {pkgs, lib, ...}@args:
    {
      imports = [ ./baseline.nix ];

      # We still want to be able to boot, adjust as needed based on your setup
      boot = {
        loader = {
          systemd-boot.enable = true;
          efi.canTouchEfiVariables = true;
        };
        kernelParams = [ "nomodeset" ];
      };
      fileSystems = {
        "/" = {
          device = "/dev/disk/by-uuid/${rootUuid}";
        };
      };
      swapDevices = [ { device = "/dev/disk/by-uuid/${swapUuid}"; } ];
      boot.initrd.availableKernelModules = [ "xhci_pci" "ehci_pci" "ahci" "usbhid" "usb_storage" "sd_mod" ];

      # Tell NixOps how to find the machine
      deployment.targetEnv = "none";
      deployment.targetHost = host;
      deployment.targetPort = port;
      networking.privateIPv4 = host;

      boot.kernelModules = [ "kvm-amd" "kvm-intel" ];
      virtualisation.libvirtd.enable = true;

      # Use the base image from `image.nix` whenever a disk does not
      # already exist, and send it to each host that needs it.
      environment.etc."virt/base-images/baseline.qcow2".source = "${import ./image.nix args}/baseline.qcow2";

      systemd.services = lib.mapAttrs' (name: guest: lib.nameValuePair "libvirtd-guest-${name}" {
        after = [ "libvirtd.service" ];
        requires = [ "libvirtd.service" ];
        wantedBy = [ "multi-user.target" ];
        serviceConfig = {
          Type = "oneshot";
          RemainAfterExit = "yes";
        };
        script =
          let
            xml = pkgs.writeText "libvirt-guest-${name}.xml"
              ''
                <domain type="kvm">
                  <name>${name}</name>
                  <uuid>UUID</uuid>
                  <os>
                    <type>hvm</type>
                  </os>
                  <memory unit="GiB">${guest.memory}</memory>
                  <devices>
                    <disk type="volume">
                      <source volume="guest-${name}"/>
                      <target dev="vda" bus="virtio"/>
                    </disk>
                    <graphics type="spice" autoport="yes"/>
                    <input type="keyboard" bus="usb"/>
                    <interface type="direct">
                      <source dev="${hostNic}" mode="bridge"/>
                      <mac address="${guest.mac}"/>
                      <model type="virtio"/>
                    </interface>
                  </devices>
                  <features>
                    <acpi/>
                  </features>
                </domain>
              '';
          in
            ''
              if [[ "$(virsh pool-list | grep guests -c)" -ne "1" ]]; then
                virsh pool-define-as --name guests --type dir --target '/dev/pool'
                virsh pool-autostart guests 
                virsh pool-build guests 
                virsh pool-start guests 
              fi

              if ! ${pkgs.libvirt}/bin/virsh vol-key 'guest-${name}' --pool guests &> /dev/null; then
                ${pkgs.libvirt}/bin/virsh vol-create-as guests 'guest-${name}' '${guest.diskSize}GiB'
                ${pkgs.qemu}/bin/qemu-img convert /etc/virt/base-images/baseline.qcow2 '/dev/pool/guest-${name}'
              fi

              uuid="$(${pkgs.libvirt}/bin/virsh domuuid '${name}' || true)"
              ${pkgs.libvirt}/bin/virsh define <(sed "s/UUID/$uuid/" '${xml}')
              ${pkgs.libvirt}/bin/virsh start '${name}'
            '';
        preStop =
          ''
            ${pkgs.libvirt}/bin/virsh shutdown '${name}'
            let "timeout = $(date +%s) + 10"
            while [ "$(${pkgs.libvirt}/bin/virsh list --name | grep --count '^${name}$')" -gt 0 ]; do
              if [ "$(date +%s)" -ge "$timeout" ]; then
                # Meh, we warned it...
                ${pkgs.libvirt}/bin/virsh destroy '${name}'
              else
                # The machine is still running, let's give it some time to shut down
                sleep 0.5
              fi
            done
          '';
      }) guests;

    };
in
  {
    # Tell NixOps about the hosts it should manage
    brute = machine {
      host = "172.17.1.11";
      rootUuid = "a4fb703c-0e04-4b30-929b-d68d6bda6176";
      swapUuid = "91c76142-80ef-4e38-ac53-391d4ab470d0";
      hostNic = "eno2";
      guests = {
        some-brute-guest = {
          memory = "2"; # GB
          diskSize = "20"; # GB
          mac = "D2:91:69:C0:14:9A";
          ip = "172.17.1.27"; # Ignored, only for personal reference
        };
      };
    };
    #truck = machine {
      #host = "192.168.0.3";
      #name = "rome";
      #hostNic = "enp3s0";
    #};
  }
