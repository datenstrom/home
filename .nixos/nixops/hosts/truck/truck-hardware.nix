{ config, lib, pkgs, ... }:

{
  imports =
    [ <nixpkgs/nixos/modules/installer/scan/not-detected.nix>
    ];

  boot.initrd.availableKernelModules = [ "ata_generic" "uhci_hcd" "ehci_pci" "ahci" "usbhid" "usb_storage" "sd_mod" ];
  boot.kernelModules = [ ];
  boot.extraModulePackages = [ ];

  fileSystems."/" =
    { device = "/dev/disk/by-uuid/15f4618e-57ff-4e23-9efc-94f1a589b8fa";
      fsType = "ext4";
    };

 fileSystems."/var/lib/plex" =
   { device = "/dev/disk/by-uuid/2dad26d9-3b1c-4b22-a3eb-7347850d9058";
     fsType = "btrfs";
   };

  boot.initrd.supportedFilesystems = [ "btrfs" ];

  swapDevices =
    [ { device = "/dev/disk/by-uuid/fc2a5390-a8ed-43e1-8088-a67a4db7bd50"; }
      { device = "/dev/disk/by-uuid/1e37ef05-77cf-4c3d-9365-03e3efb547f3"; }
    ];

  nix.maxJobs = lib.mkDefault 2;
}
