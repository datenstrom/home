# Configuration file for starshell.io
{ config, pkgs, lib, ... }:
{

  boot.loader.grub = {
    enable = true;
    version = 2;
    device = "/dev/sda";
  };

  networking.hostName = "brute";
  time.timeZone = "America/New_York";

  imports =
    [
      <nixpkgs/nixos/modules/profiles/headless.nix>
      ./brute-hardware.nix
      ../baseline.nix
    ];

  environment.systemPackages = with pkgs; [
    vim
  ];

  networking.firewall.allowPing = true;
  networking.firewall.allowedTCPPorts = [
    22 # SSH
    80 # HTTP
    389 # LDAP
    443 # HTTPS
  ];

  programs.bash.enableCompletion = true;

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;
  services.openssh.permitRootLogin = "yes";

  system.stateVersion = "18.03";
}
