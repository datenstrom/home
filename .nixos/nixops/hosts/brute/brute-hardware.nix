{ config, lib, pkgs, ... }:
{
  # IP settings
  #networking.interfaces.enp3s0 = {
  #  ip4 = [
  #    {
  #      address = "91.121.89.48";
  #      prefixLength = 24;
  #    }
  #  ];
  #  ip6 = [
  #    {
  #      address = "2001:41D0:1:8E30::";
  #      prefixLength = 64;
  #    }
  #  ];
  #};

  #networking.defaultGateway = "91.121.89.254";
  #networking.defaultGateway6 = "2001:41D0:1:8Eff:ff:ff:ff:ff";
  #networking.nameservers = [ "213.186.33.99" ];

  imports =
    [  <nixpkgs/nixos/modules/installer/scan/not-detected.nix>
    ];

  boot.extraModulePackages = [ ];
  boot.initrd.availableKernelModules = [ "ehci_pci" "ata_piix" "megaraid_sas" "usbhid" "usb_storage" "sd_mod" "sr_mod" ];
  boot.kernelModules = [ "kvm-intel" ];
  
  fileSystems."/" =
    { device = "/dev/disk/by-uuid/a4fb703c-0e04-4b30-929b-d68d6bda6176";
      fsType = "ext4";
    };

  #fileSystems."/var" =
  #  { device = "/dev/disk/by-uuid/a22a0186-5503-473a-0090-5ead3e1674e8";
  #    fsType = "ext4";
  #  };

  swapDevices =
    [  { device = "/dev/disk/by-uuid/91c76142-80ef-4e38-ac53-391d4ab470d0"; }
    ];

  nix.maxJobs = lib.mkDefault 8;
  powerManagement.cpuFreqGovernor = "ondemand";
}
