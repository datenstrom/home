# Configuration file for starshell.io
{ config, pkgs, lib, ... }:
{

  networking.hostName = "dynamite";
  time.timeZone = "America/New_York";
  #networking.hostId = "385cabe4";

  imports =
    [
      #<nixpkgs/nixos/modules/profiles/headless.nix>
      #../baseline.nix
      ../rpi_aarch64_base.nix
    ];

    #environment.systemPackages = with pkgs; [
      #vim
      #];

  #networking.firewall.allowPing = true;
  #networking.firewall.allowedTCPPorts = [
    #  22 # SSH
    #80 # HTTP
    #389 # LDAP
    #443 # HTTPS
    #];

  programs.bash.enableCompletion = true;

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;
  services.openssh.permitRootLogin = "yes";

  #system.stateVersion = "18.03";
}
