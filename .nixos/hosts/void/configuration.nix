{ config, pkgs, ... }:

{

  networking.hostName = "void";

  imports = [
      ./hardware-configuration.nix
      ../../config/workstation.nix
      ../../services/x11/x11-laptop.nix
    ];

  boot.loader.grub = {
    enable = true;
    version = 2;
    device = "/dev/sda";
  };

  boot.initrd.luks.devices = [
    {
      name = "root";
      device = "/dev/disk/by-uuid/760cd9c6-34dc-4853-ac39-132823c41114";
      preLVM = true;
      allowDiscards = true;
    }
  ];

  services = {
    xserver.desktopManager.wallpaper.mode = "fill";

    # Norfolk
    redshift.enable = true;
    redshift.latitude = "36.850769";
    redshift.longitude = "-76.285873";
  };

  environment.systemPackages = with pkgs; [
    gnome3.eog
	  aegisub
  ];

}
