{ config, pkgs, ... }:

{

  imports =
    [
      ./x11-common.nix
    ];

  # Enable touchpad support.
  services.xserver.libinput.enable = true;

  services.xserver.displayManager.sddm.enable = true;
  services.xserver.desktopManager.plasma5.enable = true;
  services.xserver.desktopManager.lumina.enable = true;
  services.xserver.windowManager.xmonad.enable = true;
  services.xserver.desktopManager.kodi.enable = true;

  # For plasma5 audio
  environment.systemPackages = with pkgs; [
    amarok
    plasma-pa
    kmix
  ];

}
