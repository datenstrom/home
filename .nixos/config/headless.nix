# Headless
#
# This configuration defines the base for any system without X11,
# and that will not have dotfiles in `$HOME`.
{ config, pkgs, ... }:

{

  imports =
    [
      ./common.nix
    ];

  programs = {
    bash.enableCompletion = true;
    command-not-found.enable = true;
    #bash.shellInit = '' '';
  };

}
