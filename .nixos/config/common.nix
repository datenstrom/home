{ pkgs, config, ... }:

let
  nvim = pkgs.neovim.override {
    vimAlias = false;
    configure = (import ./vim/vim_config.nix { pkgs = pkgs; });
  };
in
{

  services.nixosManual.showManual = true;
  nixpkgs.config.allowBroken = false;
  nixpkgs.config.allowUnfree = false;

  # Don't forget to set a password with ‘passwd’.
  users.extraUsers.datenstrom = { 
    description = "Derek Goddeau";
    isNormalUser = true;
    home = "/home/datenstrom";
    createHome = true;
    group = "datenstrom";
    extraGroups = ["wheel" "networkmanager" "audio"];
    uid = 1000;
    initialHashedPassword = "$6$a.QL3Tf.9OF5g.f$DOMsneN7GT1doBhUPC9GzWdtC/NpY6q55QQhmjyaAzLLIOlxExuLB9sWeMTpLmnBERq2X7EbzYcV7C.c1iEd31";
  };

  environment.shellAliases = {
    vi = "vim";
  };


  environment.systemPackages = with pkgs; [
    nvim
    (import ./vim/vim.nix)
    networkmanager
    file
    pciutils
    less
    mkpasswd
  ];
  programs.vim.defaultEditor = true;
  networking.networkmanager.enable = true;

  security = {
    hideProcessInformation = true;
    sudo.enable = true;
    sudo.wheelNeedsPassword = true;
  };

  boot = {
    cleanTmpDir = true;
    loader.timeout = 3;
    #loader.grub.font = "./fonts/FuraMono-Regular-Powerline.otf";

    # xpm.gz file type
    # 640x480
    # 14 colors only
    #loader.grub.splashImage = "";
  };

  time.timeZone = "America/New_York";
  i18n = {
    consoleUseXkbConfig = true;
    consoleFont = "Lat2-Terminus16";
    consoleColors = [
      "002b36" # Base 03
      "073642" # Base 02
      "586e75" # Base 01
      "657b83" # Base 00
      "839496" # Base 0
      "93a1a1" # Base 1
      "eee8d5" # Base 2
      "fdf6e3" # Base 3
      "b58900" # Yellow
      "cb4b16" # Orange
      "dc322f" # Red
      "d33682" # Magenta
      "6c71c4" # Violet
      "268bd2" # Blue
      "2aa198" # Cyan
      "859900" # Green
    ];

    defaultLocale = "en_US.UTF-8";
  };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "17.09"; # Did you read the comment?

}
