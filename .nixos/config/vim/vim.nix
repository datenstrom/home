with import <nixpkgs> {};

let
  vimrc = import ./vimrc.nix { pkgs = pkgs; };
  plugins = import ./plugins.nix { pkgs = pkgs; };
in
with pkgs;
vim_configurable.customize {
  # Specifies the vim binary name.
  #
  # E.g. set this to "my-vim" and you need to type "my-vim" to open
  # this vim. This allows to have multiple vim packages installed
  # (e.g. with a different set of plugins)
  name = "vim";

  vimrcConfig.customRC = vimrc.config;

  # Plugins
  #
  # Use the default plugin list shipped with nixpkgs
  # They are installed managed by `vam` (a vim plugin manager)
  # For a list of nixified plugins see:
  # https://github.com/NixOS/nixpkgs/blob/master/pkgs/misc/vim-plugins/vim-plugin-names
  #
  # NOTES:
  #
  # YouCompleteMe completion engines
  #  - Rust: racer
  #  - Python: jedi
  #  - C-like: clang
  #  - go: goCode & goDef
  #
  vimrcConfig.vam.knownPlugins = pkgs.vimPlugins // plugins.customPlugins;
  vimrcConfig.vam.pluginDictionaries = plugins.std ++ plugins.vim;
}
