{ pkgs }:

{

  std = [
      "nerdtree"                # File tree navigation
      "nerdtree-git-plugin"     # Show git status in NerdTree
      "ctrlp"                   # Full path fuzzy file finder
      "airline"                 # Status bar
      "vim-airline-themes"      # Status bar themes
      "Solarized"               # Solarized colorscheme
      "Spacegray-vim"           # Xcode like colorscheme
      "vim-devicons"            # File type glyphs/icons
      "vim-indent-guides"       # Indent guides
      "vim-trailing-whitespace" # Highlight/fix trailing whitespace
      "vim-signature"           # Toggle, display and navigate marks
      "vim-signify"             # Shows git diff markers
      "python-mode"             # Python everything
      "rainbow_parentheses"     # (((((((LISP)))))))
      "vimtex"                  # Lightweight LaTeX support
      "latex-live-preview"      # [ LaTeX | Preview ]
      "vim-markdown"            # Markdown support
      "vim-addon-nix"           # Nix functional language syntax, etc.
      "Syntastic"               # Syntax checking for many languages
      "fugitive"                # Git wrapper
      "rust-vim"                # Rust formatting, Syntastic integration, etc.
      "vim-racer"               # Rust Code Completion
  ];

  vim = [
      "youcompleteme"           # Fast, as-you-type, fuzzy-search code completion
  ];

  nvim = [
      "goyo"                    # Distraction free editing
      "limelight-vim"           # Hyper Focus
      "deoplete-nvim"           # Dark powered completion for neovim/vim8
      "deoplete-rust"           # Rust deoplete completion via racer
  ];

  robotics = [
    "vim-ros"                   # Robotics Operating System
  ];

  customPlugins = {
    vim-ros = pkgs.vimUtils.buildVimPlugin {
      name = "vim-ros";
      src = pkgs.fetchFromGitHub {
        owner = "taketwo";
        repo = "vim-ros";
        rev = "89824996e97a8e1f773a3559da01e564122bb079";
        sha256 = "1gki42q52bgs3z0pxd1c1aw9c1h2mv9fn31pv6h330iciy5imrw7";
      };
    };
  };

}
