# NixOs System Configs

- `config` is where specific configuration types inherit form `common.nix` and build on it. Because these are meant to be universal across all systems of that type they are conservative.
  - `common.nix` is the base for all systems.
  - `workstation.nix` is the base for development systems.
- `services` are configurations for services that will only be wanted on some systems like X11.
- `meta` contains meta-packages.
  - Collections that will only be wanted on some systems.
  - For example programs too heavy for laptops.
- `hosts` contains the `configuration.nix` and `hardware-configuration.nix` for every host.
  - Every host inherits from a single `config/type` where `type` is the purpose of the system.
  - Each host may or may not include a _service_ or _meta-package_.
- `~/.config/nixpkgs/overlays` contains a fix for Rust.
- `nixops` contains server deployment configurations.

Both `/etc/nixos/configuration.nix` and `/etc/nixos/hardware-configuration.nix` are symlinked to the appropriate files in `/etc/nixos/hosts`.

## Install Nixos with Raid & LVM

&nbsp;

**First check if the drives are Btrfs and not Mdadm.**

&nbsp;

Remove old partitions/mdadm setups:

```bash
lvremove /dev/myvolgrp/home
lvremove /dev/myvolgrp/system
lvremove /dev/myvolgrp/swap
vgremove myvolgrp
pvremote /dev/md0
mdadm --stop /dev/md0
mdadm --zero-superblock /dev/sda1
mdadm --zero-superblock /dev/sdb1
```

&nbsp;

Create the partitions, and include the swap partition here (not inside LVM):

```bash
parted /dev/sda
mklabel gpt
mkpart non-fs 0 2
mkpart primary 2 3001G
p
Number Start End Size File system Name Flags
1 17.4kB 2000kB 1983kB non-fs
2 2097kB 3001GB 3001GB primary

set 1 bios_grub on
p
Number Start End Size File system Name Flags
1 17.4kB 2000kB 1983kB non-fs bios_grub
2 2097kB 3001GB 3001GB primary
```

Create the new mdadm softraid device:

```bash
mdadm --create /dev/md0 --level=1 --raid-devices=2 /dev/sda2 /dev/sdb2
mdadm: Note: this array has metadata at the start and
may not be suitable as a boot device. If you plan to
store '/boot' on this device please ensure that
your boot-loader understands md/v1.x metadata, or use
--metadata=0.90
Continue creating array? y
mdadm: Defaulting to version 1.2 metadata
mdadm: array /dev/md0 started.
```

Create the LVM+filesystems:

```bash
pvcreate /dev/md0
Physical volume "/dev/md0" successfully created

vgcreate myVolGrp /dev/md0
Volume group "myVolGrp" successfully created

lvcreate -n system -L50G myVolGrp
lvcreate -n swap -L8G myVolGrp

mkfs.ext4 -O dir_index -j -L system /dev/myVolGrp/system
mkswap -L swap /dev/myVolGrp/swap
```

## Rust Toolchain

The current version (1.3.0) of `rustup` packaged for NixOs is broken, attempting to install rust stable results in:

```sh
--- ~ » rustup update stable
info: syncing channel updates for 'stable-x86_64-unknown-linux-gnu'
223.6 KiB / 223.6 KiB (100 %) 207.2 KiB/s ETA:   0 s
error: missing key: 'url'
```

And self update is disabled:

```sh
--- ~ » rustup self update
error: self-update is disabled for this build of rustup
error: you should probably use your system package manager to update rustup
```

See:

  - [Provide post-unpack and/or post-download hooks](https://github.com/rust-lang-nursery/rustup.rs/issues/673)
  - [Better NixOS support](https://github.com/rust-lang-nursery/rustup.rs/issues/1241)
  - [nixpkgs-mozilla](https://github.com/mozilla/nixpkgs-mozilla#rust-overlay)
  - [Rust prebuilt package overlay](https://nixos.org/nix-dev/2017-March/022947.html)
