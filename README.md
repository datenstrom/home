# Home

Dotfiles and system configuration.

## Making a new machine just like home

```bash
curl -sSL https://datenstrom.gitlab.io/home/bootstrap | bash
install_dev
```

Installs:

- [terminator](https://terminator-gtk3.readthedocs.io/en/latest/)
- [alacritty](https://github.com/alacritty/alacritty) (Pop! OS only)
- [zsh](https://www.zsh.org/)
- [docker](https://www.docker.com/)
- [gnupg2](https://gnupg.org/)
- [Rust](https://www.rust-lang.org/en-US/install.html)
- [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh#basic-installation)
- [keybase](https://keybase.io/docs/the_app/install_linux)
- [vim](https://gitlab.com/starshell/scripting/install_scripts) (With Python 3, without ROS)
- [ripgrep](https://github.com/BurntSushi/ripgrep)
- [tokei](https://github.com/XAMPPRocky/tokei)
- [xcp](https://github.com/tarka/xcp)
- [tmux](https://github.com/tmux/tmux/wiki)
- [keybase](https://keybase.io/)
- [Julia](https://julialang.org/)
- [Miniconda](https://docs.conda.io/en/latest/index.html)
- [Go](https://golang.org/)
- [Keepass](https://keepass.info/)

## Setup keys

Import PGP keys and run `config crypt unlock`. During `gpg --edit-key` use the command `trust` to set the key trust level.

```bash
keybase pgp export -q 93365252FCC1BC04 --secret -o tmp.secret
cat tmp.secret | gpg --allow-secret-key-import --import
rm tmp.secret
gpg --edit-key 93365252FCC1BC04
git-crypt add-gpg-user 93365252FCC1BC04
git-crypt unlock
```

See [git-crypt](https://www.agwa.name/projects/git-crypt/) for more info.

## Roadmap

Things to do next time I am free to hack on this for a weekend:

- Add `install_headless` or maybe flags
- Fix running `install_dev` after bootstrap or add message about sourcing
- Find a keybase replacement ([Keybase joins Zoom](https://keybase.io/blog/keybase-joins-zoom))
- [Starship](https://starship.rs/): Sync shell prompt between bash, zsh, and ion
- [fd](https://github.com/sharkdp/fd): Possible `find` replacement
- htop or [glances](https://nicolargo.github.io/glances/): Better top command
- [asciinema](https://asciinema.org/): For recording term sessions
- [tldr](https://tldr.sh/): Probably [tealdeer](https://github.com/dbrgn/tealdeer)
- [ncdu](https://dev.yorhel.nl/ncdu): NCurses Disk Usage
- [exa](https://the.exa.website/): Modern `ls`
- [bat](https://github.com/sharkdp/bat): Modern `cat`, with syntax highlighting etc.
- [procs](https://github.com/dalance/procs): Modern `ps`
- [nomino](https://github.com/yaa110/nomino): Batch rename utility for devs
- [fw](https://github.com/brocode/fw): Workspace management
- [kubectl](https://kubernetes.io/)
- [kitty](https://sw.kovidgoyal.net/kitty/): Terminal emulator (GPU, images)
- [Nix](https://nixos.org/nix/download.html)
- [Neuron](https://neuron.zettel.page/install.html): Knowledge base

[Zettlr](https://www.zettlr.com/) (Knowledge base setup and sync) has been
abandoned by the maintainer refusing to support vim keybindings and insists
it is exclusively for non-technical leaders which leads me to believe it
will only further diverge.


## Branches

Different branches can be used for different configurations. But to fetch a branch is a little different for a bare repository. To fetch a single branch `dev` run:

```bash
git fetch origin branch dev:dev
```

To get all branches:

```bash
git fetch origin +refs/heads/*:refs/heads/*
```
